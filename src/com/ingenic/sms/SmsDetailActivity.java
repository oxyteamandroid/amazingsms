/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/AmazingSms Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.sms;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.datatransactor.elf.SmsInfo;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.sms.SmsService.SmsBinder;
import com.ingenic.sms.dao.SMSDao;
import com.ingenic.sms.utils.Utils;

public class SmsDetailActivity extends RightScrollActivity implements ServiceConnection,
        SmsCallback {

    private SmsAdapter mAdapter;
    private SMSDao mDao;
    private SmsService.SmsBinder mBinder;
    private String mAddress;
    private ListView mListView;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sms_detail);

        SmsAddress address = (SmsAddress) getIntent().getSerializableExtra("address");
        if (address == null) {
            finish();
            return;
        }

        mAddress = address.getAddress();
        mDao = SMSDao.getInstance(this);

        mAdapter = new SmsAdapter(this, R.layout.layout_sms_detail_item);
        mAdapter.addAll(mDao.getSmssWithNumber(mAddress));

        mListView = (ListView) findViewById(android.R.id.list);
        mListView.setAdapter(mAdapter);
        mListView.setSelection(mAdapter.getCount() - 1);

        bindService(new Intent(this, SmsService.class), this, BIND_AUTO_CREATE);
    }

    @Override
    protected void onDestroy() {
        unbindService(this);

        if (mBinder != null) {
            mBinder.unregisterCallback(this);
            mBinder = null;
        }

        super.onDestroy();
    }

    private static class SmsAdapter extends ArrayAdapter<SmsInfo> {

        private Context mContext;
        private int mResource;

        public SmsAdapter(Context context, int resource) {
            super(context, resource);
            mContext = context;
            mResource = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;

            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(mResource, parent, false);

                holder = new ViewHolder();
                holder.sms = (TextView) convertView.findViewById(R.id.sms);
                holder.date = (TextView) convertView.findViewById(R.id.date);
                holder.dayLayout = convertView.findViewById(R.id.day_layout);
                holder.day = (TextView) convertView.findViewById(R.id.day);
                holder.smsLayout = (LinearLayout) convertView.findViewById(R.id.sms_layout);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            SmsInfo info = getItem(position);
            long time = info.getDate();

            if (position > 0) {
                SmsInfo last = getItem(position - 1);
                if (Utils.less1Min(time, last.getDate())) {
                    holder.dayLayout.setVisibility(View.GONE);
                } else {
                    holder.dayLayout.setVisibility(View.VISIBLE);
                }
            } else {
                holder.dayLayout.setVisibility(View.VISIBLE);
            }

            if (info.getType() == 1) {
                holder.smsLayout.setGravity(Gravity.NO_GRAVITY);
                holder.sms.setBackgroundResource(R.drawable.bg_sms_received);
            } else {
                holder.smsLayout.setGravity(Gravity.RIGHT);
                holder.sms.setBackgroundResource(R.drawable.bg_sms_send);
            }
            holder.sms.setText(info.getBody());

            Date date = new Date(time);
            SimpleDateFormat format = new SimpleDateFormat(" HH:mm", Locale.getDefault());
            holder.date.setText(format.format(date));

            if (Utils.isToday(time)) {
                holder.day.setText(R.string.today);
            } else if (Utils.isYesterday(time)) {
                holder.day.setText(R.string.yesterday);
            } else {
                format = new SimpleDateFormat("MM-dd", Locale.ENGLISH);
                holder.day.setText(format.format(date));
            }

            return convertView;
        }

        private static class ViewHolder {
            LinearLayout smsLayout;
            View dayLayout;
            TextView sms;
            TextView date;
            TextView day;
        }
    }

    @Override
    public void onSmsReceived(SmsInfo info) {
        if (info.getAddress().equals(mAddress)) {
            mAdapter.add(info);
            mAdapter.notifyDataSetChanged();
            mDao.readSms(mAddress);

            mListView.setSelection(mAdapter.getCount() - 1);
        } else {
            IwdsLog.w(this, "The message is not this address send.Ignore...");
        }
    }

    @Override
    public void onConnectStateChanged(boolean isConnected) {}

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mBinder = (SmsBinder) service;
        mBinder.registerCallback(this);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        if (mBinder != null) {
            mBinder.unregisterCallback(this);
            mBinder = null;
        }
    }
}