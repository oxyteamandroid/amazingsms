/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/AmazingSms Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.sms;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.media.RingtoneManager;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Vibrator;

import com.ingenic.iwds.DeviceDescriptor;
import com.ingenic.iwds.app.Note;
import com.ingenic.iwds.app.NotificationProxyServiceManager;
import com.ingenic.iwds.common.api.ConnectFailedReason;
import com.ingenic.iwds.common.api.ServiceClient;
import com.ingenic.iwds.common.api.ServiceManagerContext;
import com.ingenic.iwds.datatransactor.DataTransactor.DataTransactResult;
import com.ingenic.iwds.datatransactor.elf.SmsInfo;
import com.ingenic.iwds.datatransactor.elf.SmsTransactionModel;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.sms.dao.SMSDao;
import com.ingenic.sms.utils.Utils;

public class SmsService extends Service implements SmsTransactionModel.SmsTransactionCallback,
        ServiceClient.ConnectionCallbacks {

    private static final int MSG_RESET_VIBRATABLE = 0;

    private static SmsTransactionModel sModel;

    private SMSDao mDao;
    private ArrayList<SmsCallback> mCallbacks = new ArrayList<SmsCallback>();
    private static ServiceClient mClient;
    private NotificationProxyServiceManager mNotificationManager;
    private volatile boolean mIsFront = false;
    private volatile boolean mVibratable = true;

    private Handler mHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
            case MSG_RESET_VIBRATABLE:
                mVibratable = true;
                break;
            default:
                break;
            }
        };
    };

    @SuppressLint("NewApi")
    @Override
    public void onCreate() {
        super.onCreate();

        if (mClient == null) {
            mClient = new ServiceClient(this, ServiceManagerContext.SERVICE_NOTIFICATION_PROXY,
                    this);
        }
        mClient.connect();

        if (sModel == null) {
            sModel = new SmsTransactionModel(this, this, Utils.UUID);
        }
        sModel.start();

        Notification.Builder builder = new Notification.Builder(this);
        Notification notification = builder.build();
        startForeground(9999, notification);

        mDao = SMSDao.getInstance(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new SmsBinder(this);
    }

    @Override
    public void onDestroy() {
        if (sModel != null) {
            sModel.stop();
        }

        if (mClient != null) {
            mClient.disconnect();
        }

        super.onDestroy();
    }

    private void registerCallback(SmsCallback callback) {
        if (mCallbacks.contains(callback)) {
            IwdsLog.w(this, "This callback is aleady registered.Ignore!");
            return;
        }

        synchronized (mCallbacks) {
            mCallbacks.add(callback);
        }
    }

    private void setFront(boolean isFront) {
        mIsFront = isFront;
    }

    private void unregisterCallback(SmsCallback callback) {
        if (mCallbacks.contains(callback)) {
            synchronized (mCallbacks) {
                mCallbacks.remove(callback);
            }
        }
    }

    public static class SmsBinder extends Binder {
        private SmsService mService;

        private SmsBinder(SmsService service) {
            mService = service;
        }

        public void registerCallback(SmsCallback callback) {
            if (mService != null) {
                mService.registerCallback(callback);
            }
        }

        public void unregisterCallback(SmsCallback callback) {
            if (mService != null) {
                mService.unregisterCallback(callback);
            }
        }

        public void setFront(boolean isFront) {
            if (mService != null) {
                mService.setFront(isFront);
            }
        }

        public void clearNotifications() {
            if (mService != null) {
                mService.clearNotifications();
            }
        }
    }

    private void clearNotifications() {
        if (mNotificationManager != null) {
            Note note = new Note("Clear By PackageName", "");
            mNotificationManager.notify(-1, note);
        }
    }

    @Override
    public void onRequest() {}

    @Override
    public void onRequestFailed() {}

    @Override
    public void onObjectArrived(SmsInfo info) {
        mDao.insert(info);

        for (int i = mCallbacks.size() - 1; i >= 0; i--) {
            mCallbacks.get(i).onSmsReceived(info);
        }

        if (!mIsFront && info.getType() == 1) {
            // 播放提示音
            RingtoneManager.getRingtone(getApplicationContext(),
                    RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)).play();

            // 震动一下
            if (mVibratable) {
                Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
                vibrator.vibrate(new long[] { 0, 500, 1000 }, -1);
                mVibratable = false;
                mHandler.sendEmptyMessageDelayed(MSG_RESET_VIBRATABLE, 5000);
            }

            if (mNotificationManager != null) {
                Intent it = new Intent(this, SMSListActivity.class);
                PendingIntent intent = PendingIntent.getActivity(this, 0, it, 0);
                String person = info.getPerson();
                String title = person == null ? info.getAddress() : person;
                Note note = new Note(title, info.getBody(), intent);
                mNotificationManager.notify(info.getId(), note);
            }
        }
    }

    @Override
    public void onLinkConnected(DeviceDescriptor descriptor, boolean isConnected) {}

    @Override
    public void onChannelAvailable(boolean isAvailable) {
        for (SmsCallback callback : mCallbacks) {
            callback.onConnectStateChanged(isAvailable);
        }
    }

    @Override
    public void onSendResult(DataTransactResult result) {}

    @Override
    public void onConnected(ServiceClient serviceClient) {
        mNotificationManager = (NotificationProxyServiceManager) serviceClient
                .getServiceManagerContext();
    }

    @Override
    public void onDisconnected(ServiceClient serviceClient, boolean unexpected) {
        mNotificationManager = null;
    }

    @Override
    public void onConnectFailed(ServiceClient serviceClient, ConnectFailedReason reason) {}
}
