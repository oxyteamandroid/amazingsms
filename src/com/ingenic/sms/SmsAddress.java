/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/AmazingSms Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.sms;

import java.io.Serializable;

public class SmsAddress implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String address;
    public String name;
    public String lastSms;
    public long date;
    public int smsCount;
    public boolean hasUnread;

    public SmsAddress(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }
}