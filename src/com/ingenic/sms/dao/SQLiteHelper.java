/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/AmazingSms Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.sms.dao;

import com.ingenic.sms.utils.Utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "sms";
    private static final int VERSION = 1;
    private static final String DB_CREATE = "CREATE TABLE " + Utils.TABLE + " (" + SmsColumns._ID
            + " INTEGER PRIMARY KEY, " + SmsColumns.THREAD_ID + " INTEGER, " + SmsColumns.ADDRESS
            + " TEXT, " + SmsColumns.PERSON + " TEXT, " + SmsColumns.BODY + " TEXT, "
            + SmsColumns.TYPE + " INTEGER, " + SmsColumns.READ + " INTEGER, " + SmsColumns.PROTOCOL
            + " INTEGER, " + SmsColumns.DATE + " INTEGER);";
    private static final String DB_DROP = "DROP TABLE IF EXISTS " + Utils.TABLE;

    public SQLiteHelper(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(DB_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DB_DROP);
        onCreate(db);
    }

}
