/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/AmazingSms Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.sms;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.ingenic.iwds.app.AmazingDialog;
import com.ingenic.iwds.app.RightScrollActivity;
import com.ingenic.iwds.datatransactor.elf.SmsInfo;
import com.ingenic.iwds.utils.IwdsLog;
import com.ingenic.iwds.widget.AdapterView;
import com.ingenic.iwds.widget.AmazingSwipeListView;
import com.ingenic.iwds.widget.AmazingToast;
import com.ingenic.sms.SmsService.SmsBinder;
import com.ingenic.sms.dao.SMSDao;
import com.ingenic.sms.utils.Utils;

public class SMSListActivity extends RightScrollActivity implements ServiceConnection, SmsCallback,
        AdapterView.OnItemClickListener, AmazingSwipeListView.OnItemDeleteListener {

    static final String TAG = SMSListActivity.class.getSimpleName();
    private AmazingSwipeListView mListView;
    private SmsService.SmsBinder mBinder;
    private SMSDao mDao;
    private SmsAddressAdapter mAdapter;

    private AmazingDialog mDeleteDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bindService(new Intent(this, SmsService.class), this, BIND_AUTO_CREATE);

        mListView = (AmazingSwipeListView) findViewById(R.id.list);
        mListView.addHeaderView(createTitleView());
        mListView.setEmptyView(findViewById(android.R.id.empty));
        mDao = SMSDao.getInstance(this);
        mAdapter = new SmsAddressAdapter(this, R.layout.layout_sms_list_item);
        mListView.setAdapter(mAdapter);
        mListView.setOnItemClickListener(this);
        mListView.setOnItemDeleteListener(this);
        mListView.setSelector(R.drawable.list_item_selector);

        loadSms();

        mDeleteDialog = new AmazingDialog(this).setNegativeButton(
                AmazingDialog.USE_NORMAL_DRAWABLE, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mDeleteDialog.dismiss();
                    }
                });
    }

    private void loadSms() {
        mAdapter.clear();
        List<SmsAddress> addresses = mDao.getNumbers();
        if (addresses != null) {
            mAdapter.addAll(addresses);
        }
        mAdapter.notifyDataSetChanged();
    }

    private View createTitleView() {
        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_sms_list_title, mListView, false);
        return view;
    }

    @Override
    protected void onDestroy() {
        unbindService(this);

        if (mBinder != null) {
            mBinder.setFront(false);
            mBinder.unregisterCallback(this);
            mBinder = null;
        }

        super.onDestroy();
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        mBinder = (SmsBinder) service;
        mBinder.clearNotifications();
        mBinder.setFront(true);
        mBinder.registerCallback(this);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        if (mBinder != null) {
            mBinder.setFront(false);
            mBinder.unregisterCallback(this);
            mBinder = null;
        }
    }

    @Override
    public void onSmsReceived(SmsInfo info) {
        loadSms();
    }

    @Override
    public void onConnectStateChanged(boolean isConnected) {}

    private static class SmsAddressAdapter extends ArrayAdapter<SmsAddress> {

        private Context mContext;
        private int mResource;

        public SmsAddressAdapter(Context context, int resource) {
            super(context, resource);
            mContext = context;
            mResource = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) mContext
                        .getSystemService(LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(mResource, parent, false);

                holder = new ViewHolder();
                holder.address = (TextView) convertView.findViewById(R.id.address);
                holder.sms = (TextView) convertView.findViewById(R.id.sms);
                holder.date = (TextView) convertView.findViewById(R.id.date);
                holder.count = (TextView) convertView.findViewById(R.id.sms_count);
                holder.call = convertView.findViewById(R.id.call);

                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            SmsAddress address = getItem(position);
            final String addr = address.getAddress();

            holder.address.setText(address.name != null ? address.name : addr);
            holder.sms.setText(address.lastSms);
            holder.sms.setTextColor(address.hasUnread ? mContext.getResources().getColor(
                    android.R.color.holo_blue_light) : Color.WHITE);
            holder.count.setText("" + address.smsCount);

            long time = address.date;
            if (Utils.isYesterday(time)) {
                holder.date.setText(R.string.yesterday);
            } else {
                String template = Utils.isToday(time) ? "HH:mm" : "MM-dd";
                Date date = new Date(time);
                SimpleDateFormat format = new SimpleDateFormat(template, Locale.ENGLISH);
                holder.date.setText(format.format(date));
            }

            holder.call.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent it = new Intent("cn.ingenic.incall.action.OUT_CALL");
                    it.putExtra("number", addr);
                    it.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    try {
                        mContext.startActivity(it);
                    } catch (Exception e) {}
                }
            });

            return convertView;
        }

        private static class ViewHolder {
            TextView address;
            TextView sms;
            TextView date;
            TextView count;
            View call;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        getRightScrollView().disableRightScroll();

        SmsAddress address = mAdapter.getItem(position);
        Intent it = new Intent(this, SmsDetailActivity.class);
        it.putExtra("address", address);
        startActivity(it);

        mDao.readSms(address.getAddress());
        loadSms();
        parent.setSelection(position);
    }

    @Override
    public void onDelete(View view, int position) {
        if (mAdapter != null && mAdapter.getCount() > position) {
            final SmsAddress smsAddress = mAdapter.getItem(position);
            final String address = smsAddress.getAddress();
            final String addressName = smsAddress.name != null ? smsAddress.name : address;

            mDeleteDialog
                    .setContent(
                            String.format(
                                    getResources().getString(
                                            R.string.delete_tip), addressName))
                    .setPositiveButton(AmazingDialog.USE_NORMAL_DRAWABLE,
                            new View.OnClickListener() {

                                @Override
                                public void onClick(View v) {
                                    int result = mDao.deleteByAddress(address);

                                    if (-1 != result) {
                                        IwdsLog.d(TAG, "Delete Item info = "
                                                + addressName);
                                        mAdapter.remove(smsAddress);
                                        mAdapter.notifyDataSetChanged();
                                        AmazingToast.showToast(
                                                getApplicationContext(),
                                                R.string.delete_success,
                                                AmazingToast.LENGTH_SHORT,
                                                AmazingToast.BOTTOM_CENTER);
                                    } else {
                                        IwdsLog.d(TAG, "Delete failed!");
                                        AmazingToast.showToast(
                                                getApplicationContext(),
                                                R.string.delete_failed,
                                                AmazingToast.LENGTH_SHORT,
                                                AmazingToast.BOTTOM_CENTER);
                                    }
                                    mDeleteDialog.dismiss();
                                }
                            }).show();
        } else {
            IwdsLog.d(TAG, "Delete failed!");
            AmazingToast.showToast(getApplicationContext(),
                    R.string.delete_failed, AmazingToast.LENGTH_SHORT,
                    AmazingToast.BOTTOM_CENTER);
        }
    }
}
