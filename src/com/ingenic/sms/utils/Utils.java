/*
 * Copyright (C) 2015 Ingenic Semiconductor
 * 
 * LiJinWen(Kevin)<kevin.jwli@ingenic.com>
 * 
 * Elf/AmazingSms Project
 * 
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * You should have received a copy of the GNU General Public License along with this program; if
 * not, write to the Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
package com.ingenic.sms.utils;

import java.util.Calendar;
import android.content.ContentValues;

import com.ingenic.iwds.datatransactor.elf.SmsInfo;
import com.ingenic.sms.dao.SmsColumns;

public class Utils {
    private static final long ONE_DAY_MILLS = 24 * 60 * 60 * 1000;
    private static final long ONE_MINUTE_MILLS = 60 * 1000;
    public static final String UUID = "fcb3bc51-f75a-eb06-ee1a-2907f966e1a5";
    public static final String TABLE = "sms";

    public static ContentValues toValues(SmsInfo info) {
        if (info == null) return null;

        ContentValues values = new ContentValues();
        values.put(SmsColumns._ID, info.getId());
        values.put(SmsColumns.THREAD_ID, info.getThreadId());
        values.put(SmsColumns.ADDRESS, info.getAddress());
        values.put(SmsColumns.PERSON, info.getPerson());
        values.put(SmsColumns.BODY, info.getBody());
        values.put(SmsColumns.TYPE, info.getType());
        values.put(SmsColumns.READ, info.getRead());
        values.put(SmsColumns.PROTOCOL, info.getProtocol());
        values.put(SmsColumns.DATE, info.getDate());
        return values;
    }

    public static boolean isToday(long time) {
        return time >= getDayMinTimeMillis(0) && time <= getDayMaxTimeMillis(0);
    }

    public static boolean isYesterday(long time) {
        return time >= getDayMinTimeMillis(-1) && time <= getDayMaxTimeMillis(-1);
    }

    private static long getDayMinTimeMillis(int dayDelta) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        c.set(year, month, day, 0, 0, 0);
        long min = c.getTimeInMillis() + ONE_DAY_MILLS * dayDelta;

        return min;
    }

    private static long getDayMaxTimeMillis(int dayDelta) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(System.currentTimeMillis());

        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        c.set(year, month, day, 23, 59, 59);
        long max = c.getTimeInMillis() + ONE_DAY_MILLS * dayDelta;

        return max;
    }

    public static boolean less1Min(long time1, long time2) {
        return Math.abs(time1 - time2) <= ONE_MINUTE_MILLS;
    }
}
